from flask import Flask
from datetime import datetime
from json import dumps
import sys
import optparse
import time
import json
import os


app = Flask(__name__)

start = int(round(time.time()))


@app.route("/")
def hello_world():

    date = str(datetime.now())
    pod_name = open("/etc/hostname", "r").read().splitlines()
    env = os.getenv('ENV')

    value = {
        "date": date,
        "pod": pod_name,
        "env": env
    }
    # Dictionary to JSON Object using dumps() method
    # Return JSON Object
    return json.dumps(value)

@app.route("/health")
def health_check():
    return json.dumps("Service is Up!")

if __name__ == '__main__':
    parser = optparse.OptionParser(usage="python simpleapp.py -p ")
    parser.add_option('-p', '--port', action='store', dest='port', help='-h')
    (args, _) = parser.parse_args()
    if args.port is None:
        # print "Missing required argument: -p/--port"
        sys.exit(1)
    app.run(host='0.0.0.0', port=int(args.port), debug=False)
