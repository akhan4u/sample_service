# sample_service

Creates a sample python microservice.

A simple MicroService that return a JSON message that contains the date & time, the pod name, the environment name (passed as argument).

```
{'date': '2021-07-14 07:26:25.246504', 'pod': 'my-pod-5b75756c48-9mvw7', 'env': 'staging'}
```

### Usage

python simpleapp.py -p 3000
