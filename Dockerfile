FROM python:3-alpine

# Install App Dependencies
RUN pip3 install flask

# Bundle App Source
COPY simpleapp.py /src/simpleapp.py

EXPOSE 3000
CMD ["python", "/src/simpleapp.py", "-p 3000"]

